Requires:  
`networkx`  
`matplotlib`  

Format of graph file:

example:  
1 2 3 4  
2 3 1

In each row the first number is a node and following numbers are its indices.  
Row (1,2,3,4) means Edge(1,2) Edge(1,3) Edge(1,4)  
Graphs are undirected: adding Edge(1,2) adds also Edge(2,1)



usage:  
`python3 app.py --help` for usage help 
