import networkx as nx
import matplotlib.pyplot as plt
import sys
import importlib

def Usage():
    usageString ="""python3 app.py [--show] [--testAtlas] [pathToGraphFile] [pathToHeuristicApproach .py file]
    --show - after computing, show loaded graph in GUI
    --testAtlas - run computing on base of over 1000 graphs with vertives up to 7 (if specified, no others args are taken into account
    pathToGraphFile - path to file with graph written in adjacency list format
    pathToHeuristicApproach .py file - path to file which contains function Heuristic(graph) returning set of vertices to inspect next 
    """ 
    print(usageString)

def DisplayGraph(g):
    nx.draw(g, with_labels=True, font_weight='bold')
    plt.show()

def LoadGraphFromFile(path):
    return nx.read_adjlist(path)

def SaveGraphToFile(graph, path):
    nx.write_adjlist(graph, path)

def GetConnectedComponents(graph):
    return [graph.subgraph(c).copy() for c in nx.connected_components(graph)]

def NaiveApproach(graph):
    return graph.nodes

def RemoveHighestDegreeFirst(graph):
    nodeDegreePairs = graph.degree(graph.nodes)
    highestDegreePair = max(nodeDegreePairs, key= lambda nodeDegreePair : nodeDegreePair[1])
    _, maxDegree = highestDegreePair
    output = filter(lambda nodeDegreePair : nodeDegreePair[1] == maxDegree, nodeDegreePairs)
    output = map(lambda nodeDegreePair : nodeDegreePair[0], output)
    output = list(output)
    return output

def CalcTreeDepth(graph, GetCandidatesForRemoval):
    if len(graph.nodes) == 0:
        return 0
    if len(graph.nodes) == 1:
        return 1
    connectedComponents = GetConnectedComponents(graph)
    if(len(connectedComponents)) == 1:
        workingGraph = connectedComponents[0]
        treeDepths = []
        for v in GetCandidatesForRemoval(workingGraph):
            graphCopy = workingGraph.copy()
            graphCopy.remove_node(v)
            treeDepths.append(CalcTreeDepth(graphCopy, GetCandidatesForRemoval))
        return 1 + min(treeDepths)
    else:
        treeDepths = []
        for connectedComponent in connectedComponents:
            treeDepths.append(CalcTreeDepth(connectedComponent, GetCandidatesForRemoval))
        return max(treeDepths)

if __name__ == "__main__":
    if(len(sys.argv) == 1 or '--help' in sys.argv):
        Usage()
        sys.exit(-1)
    if('--testAtlas' in sys.argv):
        graphsToCheck = nx.graph_atlas_g()
        numberOfAllGraphs = len(graphsToCheck)
        iteration = 0
        errors = 0
        for graph in nx.graph_atlas_g():
            iteration = iteration + 1 
            exactTreeDepth = CalcTreeDepth(graph, NaiveApproach)
            heuristicTreeDepth = CalcTreeDepth(graph, RemoveHighestDegreeFirst)
            print(f"{iteration}/{numberOfAllGraphs} - Exact TD = {exactTreeDepth} / {heuristicTreeDepth} = HeuristicTD")
            if exactTreeDepth != heuristicTreeDepth:
                print("###ERROR###")
                errors = errors + 1
                SaveGraphToFile(graph, f"errorGraph{iteration}.txt")
        successRatio = (numberOfAllGraphs - errors)/numberOfAllGraphs
        print(f"Success ratio: {successRatio}")
    else:
        path = sys.argv[len(sys.argv)-1]
        graph = LoadGraphFromFile(path)
        RemoveHighestDegreeFirst(graph)
        treeDepth = CalcTreeDepth(graph, NaiveApproach)
        heuristicTreeDepth = CalcTreeDepth(graph, RemoveHighestDegreeFirst)

        print(f"Exact tree-depth = {treeDepth}")
        print(f"Heuristic tree-depth = {heuristicTreeDepth}")
        if("--show" in sys.argv):
            DisplayGraph(graph)
